TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    nice_menu.cpp

LIBS += -lncurses -lmenu

HEADERS += \
    nice_menu.h
