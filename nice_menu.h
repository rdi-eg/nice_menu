#ifndef NICE_MENU_H
#define NICE_MENU_H

#include <string>
#include <vector>

int nice_menu(const std::vector<std::string>& entries);

#endif // NICE_MENU_H
