#include "nice_menu.h"

#include <cstdlib>
#include <curses.h>
#include <menu.h>

int nice_menu(const std::vector<std::string>& entries)
{
	std::vector<ITEM*> my_items(entries.size() + 1, nullptr);
	int c;
	MENU *my_menu;

	initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);

	for(size_t i = 0; i < entries.size(); ++i)
			my_items[i] = new_item(entries[i].c_str(), "");

	my_menu = new_menu(my_items.data());
	post_menu(my_menu);
	refresh();

	ITEM* choice = nullptr;
	while((c = getch()) != KEY_EXIT)
	{   switch(c)
		{	case KEY_DOWN:
				menu_driver(my_menu, REQ_DOWN_ITEM);
				break;
			case KEY_UP:
				menu_driver(my_menu, REQ_UP_ITEM);
				break;
			case 10: // Enter
				choice = current_item(my_menu);
				int choice_index = item_index(choice);
				unpost_menu(my_menu);
				free_menu(my_menu);
				for(size_t i = 0; i < entries.size(); ++i)
					free_item(my_items[i]);

				endwin();
				return choice_index;
		}
	}

	unpost_menu(my_menu);

	for(size_t i = 0; i < entries.size(); ++i)
			free_item(my_items[i]);

	free_menu(my_menu);
	endwin();
	return -1;
}
