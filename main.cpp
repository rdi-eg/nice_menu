#include "nice_menu.h"

#include <string>
#include <vector>
#include <iostream>

using namespace std;

int main()
{
	vector<string> choices = {
		"Start",
		"Options",
		"Exit"
	};

	int choice = nice_menu(choices);
	cout << choice << endl;
}
